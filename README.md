# AREN--8940-003 Final Project

The analysis folder includes the jupyter notebooks to:
1. run the building damage analysis - final_project_analysis.ipynb
2. map the NSI dataset to the HAZUS file - nsi_to_hazus_mapping.ipynb

The second one has two similar other folder for the two other cases - retrofit 1 and retrofit 2

The inventory_shapefiles contains the initial nsi inventory "san_francisco_buildings.zip" and the final processed shapefile for IN-CORE analysis, "san_francisco_incore_final_v6.zip". Please extract the shapefile directly into the inventory_shapefile directory or adjust the filepath in the codes for analysis.
